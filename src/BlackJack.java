//package BlackJack

import java.util.Random;
import java.util.Scanner;
import java.util.Timer;

/**
 * Created by bogus on 23.03.2017.
 */
public class BlackJack {
    public static void main(String[] args) {

        int liczbaTali =10;
        int liczbaKart =liczbaTali*52;
        Karta tablica[] = new Karta[liczbaKart];
        Random generator = new Random();        // inicjacja generatora liczb pseudolosowych
        int wygraneKrupiera=0, wygraneGracza=0;

        //inicjacja krupiera i gracz
        Reka krupier = new Reka(true);
        Reka gracz =new Reka(false);

        int znak=99;                                //informacja od gracza
        Scanner odczyt = new Scanner(System.in);    //obiekt do odebrania danych od użytkownika
        boolean czyKoniec = false;                  // flaga przechowując informacje czy nadal gramy
        GrajBlackJack wygrana = new GrajBlackJack();//sprawdza kto wygral

        //lets PLAY gra kończy się kiedy grcz wpisze więcej niż 300
        while (znak<300){
            // wyswietl menu
            //!!!!!!!!!!!!!!!!!!!!!!!!!!!!
            System.out.println("    Menu BLACKJACK");
            System.out.println("1 - hint dobierz karte");
            System.out.println("2 - krupier dobiera kartę lub karty");
            System.out.println("3 - odkryj stol");
            System.out.println("300< - Koniec gry");

            //zakończenie gry i czyszczenie stołu
            if (czyKoniec==true){
                System.out.println("Gra zakonczona");
                if(wygrana.czyWygrana(krupier,gracz)==true)
                    wygraneGracza++;
                else wygraneKrupiera++;
                gracz.odkryjKarty();
                krupier.odkryjKarty();
                System.out.println("    Twoje karty:"+gracz.getAktualnaLicznaPKT());
                gracz.wyswietlKarty();
                System.out.println("    Karty krupiera: "+krupier.getAktualnaLicznaPKT());
                krupier.wyswietlKarty();
                System.out.println("W sumie gracz wygral : "+wygraneGracza+"   a krupier wygral : "+wygraneKrupiera);
                krupier.wyczyscReke();
                gracz.wyczyscReke();
                System.out.println("stoly wyczyszczone - możesz zagrać ponownie");
                czyKoniec=false;        // czyszczenie flagi

            }

            znak=odczyt.nextInt();

            switch (znak){
                case 1: {// Hint - gracz dobier kartę
                    gracz.wylosujKarte(generator.nextInt(liczbaKart-1),true);
                    gracz.wyswietlKarty();
                    System.out.println(gracz.getAktualnaLicznaPKT());
                    if (gracz.getAktualnaLicznaPKT()>21){
                        czyKoniec=wygrana.czyWygrana(krupier,gracz);
                    }
                    break;
                }

                case 2:{//krupier dobiera karty
                    if(czyKoniec==true) {   //krupier dobiera karty do konca
                        while (krupier.getAktualnaLicznaPKT() < 17)
                            krupier.wylosujKarte(generator.nextInt(liczbaKart - 1), true);
                    }else if (krupier.getAktualnaLicznaPKT()<17)//krupier dobiera jedna karte
                        krupier.wylosujKarte(generator.nextInt(liczbaKart - 1), true);
                    krupier.wyswietlKarty();
                    if(krupier.getAktualnaLicznaPKT()>21)
                        czyKoniec=wygrana.czyWygrana(krupier,gracz);

                    break;
                }

                case 3:{//odkryj stół
                    if(czyKoniec==true) {
                        System.out.println("karty krupiera: ");
                        krupier.odkryjKarty();
                        krupier.wyswietlKarty();
                        System.out.println("karty gracza: ");
                        gracz.odkryjKarty();
                        gracz.wyswietlKarty();
                    }else System.out.println("Nie można odkryć stołu gra nadal trwa !");
                }

                    default: System.out.println("nie znaleziono polecenia");
            }
        }
    }
    //cz



}
