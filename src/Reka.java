import java.util.ArrayList;
import java.util.List;

/**
 * Created by bogus on 23.03.2017.
 */
/*
public class Reka extends Karta {
    public Reka(int a){
        super(a);
    }
    Karta mojeKaty
}
*/

public class Reka{
    int aktualnaLicznaPKT=0;            //liczba punktow przy obecnej liczbie kart
    boolean czyKrupier=false;           //czy obecna reka należy do krupiera
    int liczbaDobranychKart=0;
    List<Karta> listaKart = new ArrayList<Karta>();

    public Reka(boolean krupier){
        this.czyKrupier=krupier;
    }
    public Karta wylosujKarte(int a,boolean widoczna){
        Karta nowa = new Karta(a, widoczna);
        aktualnaLicznaPKT+=nowa.getWartoscWBlackJack();
        listaKart.add(nowa);
        liczbaDobranychKart++;
        if(czyKrupier==true&&liczbaDobranychKart>1)
            nowa.setVisible(false);
        else
            nowa.setVisible(true);
        return nowa;
    }
    //wyświetla wszystkie karty
    public void wyswietlKarty(){
        for (int i = 0; i< listaKart.size();i++){
            Karta a = listaKart.get(i);
            if(a.getVisible()==true)
                System.out.println(a.getFullName());
            else System.out.println("karta zakryta");
        }
    }
    // zmienia widocznosc wszystkich kart na true
    public void odkryjKarty(){
        for (int i = 0; i< listaKart.size();i++){
            Karta a = listaKart.get(i);
            a.setVisible(true);
        }
    }
    public int getAktualnaLicznaPKT(){return aktualnaLicznaPKT;}
    public void wyczyscReke(){listaKart.clear();}





}